import React from 'react';

export const Input = ({value, onChange, className = [], name}) => {
  return (
    <input className={["input", ...className].join(" ")}
           value = {value}
           name = {name}
           onChange = {onChange} />
    );
}
