import React from 'react';

export const Select = ({name, shedule, onPick = () => {}, className = []}) => {
  return (
    <select onChange = {onPick}
            name = {name}
            className={["select", ...className].join(" ")}>
      {shedule.map(i => <option value = {i[1]} key = {i[0]}>{i[0]}</option>)}
    </select>
  );
}
