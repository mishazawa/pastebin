const { dateAdd } = require('./help');

const get = (r) => (req, res, next) => {
  if (!req.params.id) {
    return r.last().then((data) => {
      return res.json(data);
    }).catch(next)
  }
  return r.retrieve(req.params.id).then((data) => {
    return res.json(data);
  }).catch(next);
}

const addDelay = (date) => {
  let unit = 1;
  let interval = "";

  switch (date) {
    case "1m":
      interval = 'minute';
      break;
    case "30m":
      unit = 30;
      interval = 'minute';
      break;
    case "1h":
      interval = 'hour';
      break;
    case "1d":
      interval = 'day';
      break;
    case "1w":
      interval = 'week';
      break;
    case "4w":
      interval = 'month';
      break;
    default:
      unit = "";
      interval = "";
  }

  return dateAdd(new Date(), interval, unit);
}

const create = (r) => (req, res, next) => {
  const { value, exp_date, name } = req.body;

  const paste_id = "-p" + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

  const data = {
    paste_id: paste_id,
    value: value,
    exp_time: addDelay(exp_date) || "null",
    name: name || "Untitled",
    hightlight: "null",
    author: "null",
    url: paste_id,
  };

  return r.insert(paste_id, data).then((data = {}) => {
    return res.json(data);
  }).catch(next);
}


module.exports = { get, create };
