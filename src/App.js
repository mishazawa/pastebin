import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import ReactGA from 'react-ga';

import Home from './pages/Home';
import New from './pages/New';
import View from './pages/View';
import {Header} from './components';

class App extends Component {
  componentDidMount () {
    ReactGA.initialize('UA-122456380-1');
  }

  render() {
    return (
      <Router>
        <div className = "Application">
          <Header/>
          <Route exact path="/" component={Home} />
          <Route path="/new" component={New} />
          <Route path="/p/:id?" component={View} />
        </div>
      </Router>
    );
  }
}

export default App;
