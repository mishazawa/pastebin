import React from 'react';

export const Line = ({number, data}) => {
  return (
    <div className = "line">
      <span className = "number" >{number}.</span>
      <pre className="pre"> <code data-language="javascript">{ data }</code></pre>
    </div>
  );
}
