import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { Helmet } from "react-helmet";
import ReactGA from 'react-ga';

import { Textarea, Input, Button, Select } from './../../components'
import { create } from './../../service/api';

class N extends Component {
  constructor () {
    super();

    this.state = {
      name: "untitled",
      value: "",
      exp_date: "",
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.sendDataToServer = this.sendDataToServer.bind(this);

    this.shedule = [
      ["Never", "-1"],
      ["1 min", "1m"],
      ["30 min", "30m"],
      ["1 hour", "1h"],
      ["1 day", "1d"],
      ["1 week", "1w"],
      ["1 month", "4w"],
    ]
  }

  onChangeHandler (e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  sendDataToServer () {
    if (this.state.value) {
      create(this.state).then((id) => {
        this.props.history.push(`/p/${id}`);
      });
    }
  }

  componentDidMount () {
    ReactGA.pageview(window.location.pathname + window.location.search);
  }

  render () {
    return (
      <div>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Ｎｅｗ　ｐａｓｔｅ</title>
        </Helmet>
        <div className="name-wrapper">Name: <Input value={this.state.name} onChange = {this.onChangeHandler} name = "name"/></div>
        <Textarea value = {this.state.value} onChange = {this.onChangeHandler} name = "value"/>
        <Select className = {["sheduler"]} shedule = {this.shedule} onPick = {this.onChangeHandler} name = "exp_date" />
        <Button value = "Save" type={["success"]} onClick = {this.sendDataToServer}/>
      </div>
    );
  }
}

export const New = withRouter(N);
