import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import ReactGA from 'react-ga';

import { get } from './../../service/api';
import { PasteLink } from './../../components'

export class Home extends Component {
  constructor () {
    super();
    this.state = {
      list: []
    }
  }
  componentDidMount () {
    get().then((list) => {
      this.setState({list});
      ReactGA.pageview(window.location.pathname + window.location.search);
    })
  }
  render () {
    return (
      <div className = "home-page">
        <Helmet>
          <meta charSet="utf-8" />
          <title>Kletka</title>
        </Helmet>
        <h2>Recent pastes</h2>
        {
          this.state.list.map(p => <PasteLink key={p.paste_id} paste={p}/>)
        }
      </div>
    );
  }
}
