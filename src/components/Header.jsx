import React from 'react';
import { Link } from "react-router-dom";
import favicon from "./../assets/favicon.ico";

export const Header = () => {
  return (
    <header className="header">
      <Link to="/" className="home"><img src={favicon} alt="home"/> <p>Pastebin</p></Link>
      <Link to="/new" className = "new-paste-link"> Create </Link>
    </header>
  );
}
