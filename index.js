const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');

const router = require('./server/router');
const errorHandler = require('./server/error');

const db = require('./server/redis')();

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(compression());
app.use(router(db));
app.use(errorHandler);

app.disable('x-powered-by');

process.on('SIGTERM', exit);
process.on('SIGINT', exit);

const server = app.listen(process.env.PORT || 5000, () => {
  console.log("Start...");
});

function exit () {
  server.close(() => {
    db.exit();
    console.log("Shut down");
  })
}
