import React from 'react';

export const Textarea = ({name, value, onChange, className=[]}) => {
  return (
    <textarea value = {value}
              name = {name}
              onChange = {onChange}
              className = {["textarea", ...className].join(" ")}>
    </textarea>
  );
}
