const router = require('express').Router();
const paste = require('./paste.controller');

module.exports = function (...args) {
  router.get('/api/paste/:id?', paste.get(...args));
  router.post('/api/paste', paste.create(...args));
  return router;
}

