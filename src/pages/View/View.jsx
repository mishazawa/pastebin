import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { Helmet } from "react-helmet";
import ReactGA from 'react-ga';

import { Line, Textarea } from './../../components';
import { get } from './../../service/api';

class V extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }
  componentDidMount () {
    const { match } = this.props;
    if (!match.params.id) return this.props.history.push('/');
    get(match.params.id).then((resp) => {
      if (resp === null) return this.props.history.push('/');
      this.setState({data: resp})
      ReactGA.pageview(window.location.pathname + window.location.search);
    });
  }

  render () {
    if (!this.state.data) return null;
    const { data }  = this.state;
    const fix = data.value.split(/\n/g);
    return (
      <div>
        <Helmet>
          <meta charSet="utf-8" />
          <title>{data.name}</title>
        </Helmet>
        <h1>{data.name}</h1>
        <div className = "pretty-paste">
        {
          fix.map((line, i) => {
            return <Line key={i} number = {i} data = {line}/>

          })
        }
        </div>
        <div className = "raw-paste">
          <h1>Raw paste</h1>
          <Textarea value = {data.value} onChange={() => {}}/>
        </div>
      </div>
    )
  }
}

export const View = withRouter(V);
