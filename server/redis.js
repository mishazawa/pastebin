const redis = require("redis");
const client = redis.createClient();

module.exports = function redisModule () {

  return { insert, retrieve, exit, last };

  function insert (id, data) {
    return new Promise((res, rej) => {
      return client.hmset(id, data, (err, result) => {
        if (err !== null) return rej(err);
        if (data.exp_time === "null") return res(id);
        return client.EXPIRE(id, parseInt((data.exp_time.getTime() - Date.now()) / 1000, 10), (err, result) => {
          if (err !== null) return rej(err);
          return res(id);
        });
      });
    });
  }

  function retrieve (id) {
    return new Promise((res, rej) => {
      return client.hgetall(id, (err, reply) => {
        if (err !== null) return rej(err);
        return res(reply);
      });
    });
  }

  function last () {
    return new Promise((res, rej) => {
      return client.scan(0, "COUNT", 100, "MATCH", "-p*", (err, reply) => {
        if (err !== null) return rej(err);

        const multi = client.multi();
        const [,keys] = reply;
        keys.forEach(k => multi.hgetall(k));

        return multi.exec((err, replies) => {
          if (err !== null) return rej(err);
          return res(replies);
        });
      })

    });
  }

  function exit() {
    client.quit();
  }

}
