import React from 'react';
import { Link } from "react-router-dom";

export const PasteLink = ({paste}) => {
  return (
    <div className = "paste-link">
      <Link to={`/p/${paste.paste_id}`}>{paste.name}</Link>
      <p>{paste.author === "null" ? "Anonymous" : paste.author}</p>
    </div>
  );
}
