const API = '/api/paste';


function get (id="") {
  return fetch(`${API}/${id}`).then(d => d.json()).catch(console.error);
}

function create (data) {
  return fetch(API, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data),
  })
  .then(d => d.json())
  .catch(console.error);

}

export { get, create };
