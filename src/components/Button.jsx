import React from 'react';

export const Button = ({value, onClick, type=[]}) => {
  return <button onClick={onClick} className = {["button", ...type].join(" ")}>{value}</button>
}
